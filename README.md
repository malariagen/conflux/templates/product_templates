# Product Templates

A central source repository for all Product planning and requirements templates.
Product, Page, Feature, Epic, etc...

If you wish to make a change are not the designated owner, you'll need merge approval from them.

| Template | Function | Owned by |
| ------ | ------ | ------ |
| Product | High level description and link to all pages within the named product. | Chris Jacob |
| Page | Details of web-pages within a product and a list of buildable features. | Chris Jacob |
| Feature | Key work units for story time and backlog sessions. Lists of stories and technical builds. | Chris Jacob |
| Epic | deprecated | Chris Jacob |
